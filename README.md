<br />
<p align="center">
  </a>
  <h3 align="center">Ecmastatus!</h3>
  
   <p align="center">
    A status update website thing I guess
    <br />
    <a href="https://github.com/leJad/ecmastatus"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://lz468m.deta.dev/">View Demo</a>
    ·
    <a href="https://github.com/leJad/ecmastatus/issues">Report Bug</a>
    ·
    <a href="https://github.com/leJad/ecmastatus/issues">Request Feature</a>
  </p>
</p>


<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project

![Screenshot of demo](https://i.imgur.com/l2SEhuv.png)

So you know how every social media has a status thing for you to write some random quote there and look like a fucking genius who reads books all day right?  

Yes, and that was not enough for me, so I coded a little website for myself where I can update my status.


Here's why:
* I was learning python and had no other project idea.
* I quit almost every social media platform, and there was no place for me to update my status (☍﹏⁰)｡


### Built With

This amazing, perfect, very popular and cool project is built using
* [Python](https://getbootstrap.com)
* [FastAPI](https://fastapi.tiangolo.com/)
* [Jinja2](https://jinja.palletsprojects.com/en/3.0.x/)
* [Redis](https://redis.io/)



<!-- GETTING STARTED -->
## Getting Started

If you are still interested in this project and want to know how you can install it, let's continue.

### Prerequisites

Before downloading ecmastatus with git you should install Python3, Git, Redis and FastAPI
* Install Python3 from [here](https://www.python.org/downloads/)
* Install Git from [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* Install Redis
  ```sh
  brew install redis
  ```
### Installation
1. After installing git, clone the repo
  ```sh
  git clone https://github.com/leJad/ecmastatus.git
  ```
2. Cd to project folder
  ```sh
  cd ecmastatus
  ```
3. Create virtual env
  ```sh
  python3 -m venv venv
  ```
  and activate it
  ```sh
  source venv/bin/activate
  ```
4. Install requirements
  ```sh
  pip install -r requirements.txt 
  ```
5. Start the redis server
  ```sh
  brew services start redis 
  ```
  or just
  ```sh
  redis-server
  ```

Now before running our app, you would want to create a .env file and add these variables:

host: (redis host), port (redis port), db-pass (if your redis db has a password), username (auth for /docs), passw (auth for /docs) and finally webhook-url

If you don't want an notification system, don't add webhook-url to .env go to /app delete scripts folder and files inside of it, and delete the ask_ecma route inside of app/main.py


6. Finally, run the project
  ```sh
  python3 app/main.py
  ```




<!-- USAGE EXAMPLES -->
## Usage

Other than using this project as a shitty status updater as I do, you can use it as a mini blog or something like that

Change the database, Change the frontend, Or even change the backend. Do whatever you want

<!-- ROADMAP -->
## Roadmap

See the [open issues](https://github.com/leJad/ecmastatus/issues) for a list of proposed features (and known issues).


<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the WTFPL License. See `LICENSE` for more information.

## Contact

[Let's Talk!](https://ecma.netlify.app/)
