import uvicorn, markdown

from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from starlette.responses import RedirectResponse
from scripts import notifier  # * Just a notifier script to notify me when someone joins hack.chat/?ask_ecma // you can delete this if you want but you will have to delete /ask_ecma route also!
from redisdb import db
from routers import create # * Added routes with tags "Create n Delete" to there {/add, /edit and /del}
from starlette.exceptions import HTTPException

app = FastAPI()


templates = Jinja2Templates(directory="templates")
app.mount("/static", StaticFiles(directory="static"), name="static")


app.include_router(create.router)


#* 404 Page
@app.exception_handler(HTTPException)
async def custom_http_exception_handler(request, exc):
    return templates.TemplateResponse(
        "404.html", {"request": request}
        )

#* Main page
@app.get("/", response_class=HTMLResponse, tags=["Default"])
async def main(request: Request):
    data = templates.TemplateResponse(
        "home.html", {"request": request}
    )
    return data

#* Get the latest post from redis and show the content 
@app.get("/mortal", response_class=HTMLResponse, tags=["Default"])
async def main(request: Request):
    data = templates.TemplateResponse(
        "data.html", {"request": request, "Title": "Latest", "id": markdown.markdown(db.get_latest())}
    )
    return data


#* Will search for the title in redis if found: will show the content if not found: will return an error. Example usage: /data/Yikes
@app.get("/data/{title}", response_class=HTMLResponse, tags=["Default"])
async def old_statuses(request: Request, title: str):
    data = templates.TemplateResponse(
        "data.html", {"request": request, "Title": title, "id": markdown.markdown(db.get_data(title))}
    )
    return data


#* Will show every statuses that has ever been published: keys *
@app.get("/history", response_class=HTMLResponse, tags=["Default"])
async def get_all(request: Request):
    data = templates.TemplateResponse(
        "history.html", {"request": request, "statuses": db.history()}
    )
    return data


#* Just for chatting
#! Not required, you can delete it if you want.
@app.get("/ask_ecma", tags=["Default"])
async def redirect():
    notifier.send()  # * Just a notifier script to notify me when someone joins hack.chat/?ask_ecma // you can delete this if you want
    response = RedirectResponse(url="https://www.hack.chat/?ask_ecma") #* Redirect to chat room
    return response


if __name__ == "__main__":
    uvicorn.run("main:app", port=8000, reload=True)
