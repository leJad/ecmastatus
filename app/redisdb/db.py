import redis, os

from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED
from dotenv import load_dotenv
load_dotenv()

host, db_passw, port = os.environ.get("host"), os.environ.get("db-pass"), os.environ.get("port") #* They are not actually required if you are testing this in a local environment. If so, change host to localhost and port to your port

#! Connection to redis
r = redis.Redis(host=host, port=port, decode_responses=True, password = db_passw, db=0) #* You won't need password if you are connecting from localhost!

#! Mortal = These mfs are getting overwrited when you add new one. Temporary data
#! Queen = And these mfs will be written to the. Permanent data

def add_mortal(content):
    r.set("mortal", content)
    
    return {
        "Success": 1
    }


def WriteDB(title, content):
    if r.hexists("queen", title):
        raise HTTPException(status_code=HTTP_405_METHOD_NOT_ALLOWED, detail="This status already exists!")
    r.hset("queen", title, content) #* Add new status to redis
    return {
        "Success": 1,
        "Title": title
        }


def EditDB(title, content):  # Damn, duplication. * Impostor syndrome kicks in *
    if not r.hexists("queen", title):
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="This status does not exist!")
    r.hset("queen", title, content) #* Edit status
    return {
        "Success": 1,
        "Title": title
        }


def delete_data(title):
    if not r.hexists("queen", title):
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="This status does not exist!")
    r.hdel("queen", title) #* Delete status
    return {"Success": 1}


def get_data(title: str) -> str:
    if r.hexists("queen", title):
        return r.hmget("queen", title)[0]
    raise HTTPException(
        status_code=HTTP_404_NOT_FOUND,
        detail="We couldn't find any status with the given title (▰˘︹˘▰)",
    )


def history():
    return r.hkeys("queen")


def get_latest():
    if r.exists("mortal"):
        return r.get("mortal")
    return "No new status at the moment ●︿●"