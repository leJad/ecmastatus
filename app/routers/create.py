import secrets, os

from fastapi import security, HTTPException, APIRouter, Depends, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from pydantic import BaseModel
from redisdb import db
from dotenv import load_dotenv
load_dotenv()

router = APIRouter()

security = HTTPBasic()

username, passw = os.environ.get("username"), os.environ.get("passw") # Yes I store them in .env. Please don't hurt me.

def get_current_username(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = secrets.compare_digest(credentials.username, username) #* Compare the given username to the username in .env file
    correct_password = secrets.compare_digest(credentials.password, passw) #* Compare the given password to the password in .env file
    
    #* If password or username is not correct return error 
    if not (correct_username and correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password, you are not ecma!",
            headers={"WWW-Authenticate": "Basic"},
        )
    return credentials.username


class Model(BaseModel):
    title: str
    content: str

class Mortal(BaseModel):
    content: str

@router.post("/add", tags=["Create n Delete"])
def add(model: Model, auth: str = Depends(get_current_username)):
    return db.WriteDB(model.title, model.content) #* Add new queen status


@router.post("/mortal", tags = ["Create n Delete"])
def post(model: Mortal, auth: str = Depends(get_current_username)):
    return db.add_mortal(model.content) #* Add new mortal status

@router.put("/edit", tags=["Create n Delete"])
def edit(model: Model, auth: str = Depends(get_current_username)):
    return db.EditDB(model.title, model.content) #* Edit status
    

@router.delete("/del", tags=["Create n Delete"])
def delete_data(title, auth: str = Depends(get_current_username)):
    return db.delete_data(title) #* Delete status with given title


#! Mortal = These mfs are getting overwrited when you add new one. Temporary data
#! Queen = And these mfs will be written to the. Permanent data