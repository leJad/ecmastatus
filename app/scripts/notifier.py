import requests
from dotenv import load_dotenv
load_dotenv()

import os
token = os.environ.get("webhook-url") #* Webhook of my channel. To create new one for you click on edit channel --> Webhooks --> Creates webhook --> Copy --> Paste to .env (webhook-url)

def send():
    mUrl = token

    data = {"content": 'Someone is waiting you at https://hack.chat/?ask_ecma'}
    response = requests.post(mUrl, json=data) #* Post request to url

    return response.status_code
